package 第一次个人编程作业;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Score {
	public static void main(String[] args) throws FileNotFoundException, IOException {

		// 读取本地html文本
		File sf = new File("src/small.html");
		File af = new File("src/all.html");
		Document ds = Jsoup.parse(sf, "UTF-8");
		Document da = Jsoup.parse(af, "UTF-8");

		// 读取配置文件
		Properties pf = new Properties();
		pf.load(new FileInputStream("src/total.properties"));

		// 初始化个人经验
		int before = 0;
		int base = 0;
		int test = 0;
		int program = 0;
		int add = 0;
		double total_before = Integer.parseInt(pf.getProperty("before"));
		double total_base = Integer.parseInt(pf.getProperty("base"));
		double total_test = Integer.parseInt(pf.getProperty("test"));
		double full_program = Integer.parseInt(pf.getProperty("program"));
		double full_add = Integer.parseInt(pf.getProperty("add"));

		// 云班课经验统计部分
		if (ds != null) {
			Elements elements = ds.getElementsByAttributeValue("class", "interaction-row");
			int temp;
			for (int i = 0; i < elements.size(); i++) {
				if (elements.get(i).child(1).child(0).toString().contains("课前自测")) {
					if (elements.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
						temp = sc.nextInt();
						before += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("课堂完成")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
						temp = sc.nextInt();
						base += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("课堂小测")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
						temp = sc.nextInt();
						test += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("编程题")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
						temp = sc.nextInt();
						program += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("附加题")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
						temp = sc.nextInt();
						add += temp;
					}
				} 
			}
		}
		if(da != null) {
			Elements elements = da.getElementsByAttributeValue("class", "interaction-row");
			int temp;
			for(int i = 0; i < elements.size(); i++) {
				if (elements.get(i).child(1).child(0).toString().contains("附加题")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
						temp = sc.nextInt();
						add += temp;
					}
				} 
			}
		}
		double result_before = before / total_before * 100 * 0.25;
		double result_base = base / total_base * 100 * 0.3 * 0.95;
		double result_test = test / total_test * 100 * 0.2;
		double result_program = program / full_program * 100 * 0.1;
		double result_add = add / full_add * 100 * 0.05;
		double total_score = (result_add + result_base + result_before + result_program + result_test) * 0.9 + 6;
		System.out.println(String.format("%.2f", total_score));
		
	}
}
